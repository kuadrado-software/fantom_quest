from fantomatic_engine import run_game
import os
from pathlib import Path
import sys


def main():
    # if getattr(sys, 'frozen', False) and hasattr(sys, '_MEIPASS'):
    #     os.chdir(sys._MEIPASS)

    os.environ["EXEC_DIR"] = str(Path(__file__).parent)
    run_game([])


if __name__ == "__main__":
    main()
