#!/bin/sh

pyinstaller cli.py --name fantom_quest \
    --onefile \
    --collect-datas="fantom_quest" \
    --windowed \
    --icon="icon.ico" \
    -w
