# FANTOM QUEST

This archive contains the data of the game Fantom Quest.

Fantom Quest is a 2D video game, top viewed / maze like.

The game is meant to be loaded and run with the Python package fantomatic_engine.

## Credit

-   Original concept, images and animations: Lucie Ventadour
-   Musics and sounds, game engine programming, distribution: Pierre Jarriges - Kuadrado Software

## Licenses

-   Game engine - Python Code: Fantomatic Engine is free and open sources. Published under GNU GPL v3 license.
-   Images, musics and sounds assets: All rights reserved - Pierre Jarriges & Lucie Ventadour - 2022
