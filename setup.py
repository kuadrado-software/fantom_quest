from setuptools import setup, find_packages
from glob import glob
import os
import sys

# cur_directory_path = os.path.abspath(os.path.dirname(__file__))

# data_files = []


# def include_files(dir, dest_root):
#     files = list()

#     for item in os.listdir(dir):
#         item_path = os.path.join(dir, item)
#         if os.path.isfile(item_path):
#             files.append(item_path)
#         else:
#             include_files(item_path, os.path.join(dest_root, item))
#     data_files.append((dest_root, files))


# start_point = os.path.join(
#     cur_directory_path, 'fantom_quest', '.fantomatic_data')

# include_files(start_point, os.path.join(
#     sys.prefix, ".fantomatic_data"))


setup(name="fantom_quest",
      version="1.1",
      description="A maze like 2D video game where a little ghost climbs the stages of an old dungeon.",
      long_description=open('README.md').read(),
      long_description_content_type='text/markdown',
      author="Lucie Ventadour, Pierre Jarriges, Kuadrado Software",
      author_email="contact@kuadrado-software.fr",
      keywords='game 2d videogame ghost maze labyrinth dungeon',
      url="https://kuadrado-software.fr",
      packages=find_packages(),
      #   data_files=data_files,
      install_requires=[
          "fantomatic_engine==0.1.16",
      ],
      python_requires='>=3.9',)
